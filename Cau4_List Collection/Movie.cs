﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baitap
{
    internal class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Director { get; set; }

        public Movie(int id, string name, string director)
        {
            Id = id;
            Name = name;
            Director = director;
        }
    }
}
