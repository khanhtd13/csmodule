﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baitap
{
    internal class PlayMovie
    {
        public void ShowMovie()
        {
            Console.WriteLine("a/ Khoi tao va them vao List");
            List<Movie> list = new();
            list.Add(new Movie(2,"Hello Beby","Unknown"));
            list.Add(new Movie(3,"Hello Cuty","Unknown"));
            list.Add(new Movie(5,"Goodbye Beby ","Unknown"));
            list.Add(new Movie(1,"Goodbye Cty","Unknown"));
            ShowList(list);



            list.RemoveAt(3);
            Console.ForegroundColor= ConsoleColor.Red;
            Console.WriteLine("b/ Đa XOA phan tu vi tri thu 3 trong List");



            list.Insert(2, new Movie(4, "Movie4", "Unknown"));
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("c/ Da THÊM Phan tu vi tri thu 2 vao List");



            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("d/ List sau khi sap xep");
            list.OrderByDescending(x=>x.Id);
            ShowList(list);




            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("e/ Phan tu can tim la:");
            var m = list.Where(x=>x.Name=="Movie4");
            foreach (var item in m)
            {
                foreach (var i in item.GetType().GetProperties())
                {
                    Console.WriteLine($"{i.Name}:    {i.GetValue(item)}");
                }
            }


            Console.ForegroundColor = ConsoleColor.White;

        }
        private void ShowList(List<Movie> list)
        {
            foreach (var item in list)
            {
                foreach (var i in item.GetType().GetProperties())
                {
                    Console.WriteLine($"{i.Name}:    {i.GetValue(item)}");
                }
            }
        }
    }
}
