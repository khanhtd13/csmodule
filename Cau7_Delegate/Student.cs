using System;
namespace Baitap
{
   
    public class Student
    {
        public string Name{get;set;}
        public int  StudentId{get;set;}
        public DateTime  Birth{get;set;}
        public string Gender{get;set;}
        public Student()
        {
            
        }
        public Student(int id,string name, string gender, DateTime birth)
        {
            StudentId=id;
            Name=name;
            Gender= gender;
            Birth= birth;
        }
    }
}