﻿using System.Reflection.Emit;
using System.Diagnostics.SymbolStore;
using System.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Baitap
{
    class Program
    {
    public delegate void ShowInfo(Student student);
        static void Main(string[] args)
        {
          ShowInfo show= new ShowInfo(ShowInfoStudent);
          show(InitStudent());
        }
        public static Student InitStudent(){
            var student= new Student();
            System.Console.WriteLine("Nhap thong tin Student:");
            System.Console.WriteLine("ID:");      
            student.StudentId= int.Parse(Console.ReadLine());
            System.Console.WriteLine("Name:");      
            student.Name= Console.ReadLine();
            System.Console.WriteLine("Gender:");
            student.Gender= Console.ReadLine();
            flag:
            System.Console.WriteLine("Birth:");
            try
            {
                 student.Birth= DateTime.Parse(Console.ReadLine());
            }
            catch{
                System.Console.WriteLine("Dinh dang cua Birth la: yyyy/mm/dd");
                goto flag;
            }
            return student;
        }
        public  static void ShowInfoStudent(Student student){
            foreach(var item in student.GetType().GetProperties()){
                System.Console.WriteLine($"{item.Name}:    {item.GetValue(student)}");
            }
        }

    }

}
