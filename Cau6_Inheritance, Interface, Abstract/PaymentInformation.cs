using System;
using System.Runtime.CompilerServices;
namespace Baitap
{
    public abstract class PaymentInformation
    {
        private int _paymentNo{get;set;}
         public virtual int GetPaymentNo(){
             var ran= new Random();
            return ran.Next(1000,4000);
        } 
    }
    public interface IOrder{
        void  PlaceOrder();void CancelOrder();void GetOrder();
    }
    public class PizzaOrder : PaymentInformation, IOrder
    {
        private double _price=25000;
        private int _amount=0;
        private bool _isOrdered=false;
        public int Amount{
            get{
                return _amount;
            }
            set{
                _amount=value;
            }
        }
        public PizzaOrder(int amount)
        {
            _amount=amount;
        }

        public void CancelOrder()
        {
            if (_isOrdered==true)
            {
                System.Console.WriteLine("Huy thanh cong!");
                _isOrdered=false;
            }else{
                System.Console.WriteLine("Chua dat hang!");
            }
        }

        public void GetOrder()
        {
            System.Console.WriteLine($"IsOrdered: {_isOrdered}");
            System.Console.WriteLine($"Amount: {_amount}");
            System.Console.WriteLine($"Price: {_price}");

        }

        public void PlaceOrder()
        {
           if(_amount==0){
               System.Console.WriteLine("Amount phai > 0");
               return;
           }
           if(_isOrdered==true){
               System.Console.WriteLine("Ban da dat roi!");
               return;
           }

           var total=_amount*_price;
           _isOrdered=true;
           System.Console.WriteLine($"PaymentNo: {GetPaymentNo()}     TotalPrice: {total}");

        }
    }

}