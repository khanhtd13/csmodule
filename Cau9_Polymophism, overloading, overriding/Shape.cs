﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baitap
{
    internal class Shape
    {
        public float Width { get; set; }
        public float Height { get; set; }

        public virtual float Area(float width, float height) => 0f;
    }
}
