﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Baitap
{
    internal class Rectangle : Shape
    {
        public void Size(float width, float height)
        {
            Width = width;
            Height = height;
            Console.WriteLine($"Width: {width}");
            Console.WriteLine($"Height: {height}");
        }
        public override float Area(float width, float height)
        {
            return width * height;
        }
        public static Rectangle operator +(Rectangle rec1, Rectangle rec2)
        {
            var rec = new Rectangle();
            rec.Width = rec1.Width + rec2.Width;
            rec.Height = rec1.Height + rec2.Height;
            return rec;
        }
        public static Rectangle operator -(Rectangle rec1, Rectangle rec2)
        {
            var rec = new Rectangle();
            rec.Width = rec1.Width - rec2.Width;
            rec.Height = rec1.Height - rec2.Height;
            return rec;
        }
    }
}
