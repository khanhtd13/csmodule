﻿using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using System.Diagnostics.SymbolStore;
using System.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Baitap
{
    class Program
    {
        static void Main(string[] args)
        {
            Rectangle r1 = new();
            Rectangle r2 = new();

            r1.Width = 10.1f;
            r1.Height = 15.4f;

            r2.Width = 11;
            r2.Height = 20;

            r1.Size(10,20);     // Bởi vì Hàm Size có truyền tham số và gán giá trị
                                // nên bây giờ r1.Width= 10, r1.Height= 20 thay vì r1.Width= 10.1, r1.Height= 15.4
            Console.WriteLine("---------------------");

            r2.Size(4,5);
            Console.WriteLine("---------------------");

            Console.WriteLine("Area r1:  "+r1.Area(1, 2));
            Console.WriteLine("---------------------");

            Console.WriteLine("Area r2:  "+r2.Area(3, 4));

            /*
             Nguyên nhân vì sao Area in ra kết quả không giống như của phần khai báo ở trên:
                => Vì Hàm Area có thể truyền vào tham số và tính trên tham số này 
                    chứ không hề động đến property của Shape.
             */
            // Cộng 2 Rectangle
            var addRec = new Rectangle();
            addRec = r1 + r2;

            //Trừ 2 Rectangle
            var minusRec = new Rectangle();
            minusRec = r1 - r2;

            Console.WriteLine("-----------In ra Ractangle (+) ----------");
            addRec.Size(addRec.Width, addRec.Height);

            Console.WriteLine("-----------In ra Ractangle (-) ----------");
            minusRec.Size(minusRec.Width, minusRec.Height);

        }
    }
}
