using System.Collections.Generic;

namespace Baitap
{
    public class StudentCompare : IComparer<Students>
    {
        // Khởi tạo biến này để lấy tham số là Property truyền vào
        private string _type { get; set; }
        public StudentCompare(string type)
        {
            _type = type;
        }

        //Lấy giá trị của Tham số truyền vào để có thể nhận tất cả các loại property
         private string GetStu(Students obj)
        {

                var tmp = obj.GetType().GetProperties();
                foreach (var item in tmp)
                {
                    if (item.Name.ToLower() == _type.ToLower())
                        return item.GetValue(obj) + "";
                }
                return "";
        }
        // Có thể truyền vào tất cả các loại property của Students
        public int Compare(Students x, Students y)
        {
            return GetStu(y).CompareTo(GetStu(x));
        }
    }
}