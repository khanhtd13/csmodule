using System.Collections.Generic;
using System;
using System.Collections;

namespace Baitap
{
    public class Students
    {
        public string Name{get;set;}
        public int  StudentId{get;set;}
        public int  Age{get;set;}
        public string Class{get;set;}
        public Students()
        {
            
        }
        public Students(int id,string name, int age,string cls)
        {
            StudentId=id;
            Name=name;
            Age= age;
            Class=cls;
        }
    }
}