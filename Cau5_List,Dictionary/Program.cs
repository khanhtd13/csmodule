﻿using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
using System.Reflection;
using System.Diagnostics.SymbolStore;
using System.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Baitap
{
    class Program
    {
        //Các hàm kiểm tra và xóa sẽ thực hiện chức năng dựa trên thuộc tính StudentID
        static void Main(string[] args)
        {
            System.Console.WriteLine("------------------Cau 2------------------");
            var list = ListStudent(); //Câu 2:  Gọi hàm để lấy 1 danh sách--Hàm ListStudent nó có Add rồi
            ShowList(list);             // Show ra danh sách
            Console.ForegroundColor = ConsoleColor.Yellow;

            var stu = new Students(1, "Khanh 1", 18, "fr_net");   // Tạo 1 student mới

            System.Console.WriteLine("------------------Cau 3------------------");
            IsExist(list, stu);        //Câu 3: Kiểm tra tồn tại

            Console.ForegroundColor = ConsoleColor.Cyan;

            System.Console.WriteLine("------------------Cau 4------------------");
            Sort(list, "StudentID");     //Câu 4: Sắp xếp
            ShowList(list);

            Console.ForegroundColor = ConsoleColor.DarkRed;

            System.Console.WriteLine("------------------Cau 5------------------");
            Delete(list, stu);     //Câu 5: Xóa 1 student

            Console.ForegroundColor = ConsoleColor.Green;

            System.Console.WriteLine("------------------Cau 6------------------");
            System.Console.WriteLine("So luong con lai: " + Count(list));          //Câu 6: Show ra số lượng

        }
        // Hàm để Show ra Danh sách Students
        public static void ShowList(List<Students> list)
        {
            foreach (var item in list)
            {
                var tmp = item.GetType().GetProperties();
                System.Console.WriteLine("--------------------");
                foreach (var i in tmp)
                {
                    System.Console.WriteLine($"{i.Name}:   {i.GetValue(item)}");
                }
            }
        }
        // Hàm tự tạo sẵn các student
        public static List<Students> ListStudent()
        {
            var listStudent = new List<Students>();
            listStudent.Add(new Students(2, "Truong 2", 19, "fr_test"));
            listStudent.Add(new Students(1, "Khanh 1", 18, "fr_net"));
            listStudent.Add(new Students(8, "Duy 8", 23, "fr_test"));
            listStudent.Add(new Students(4, "Khanh 4", 21, "fr_net"));
            listStudent.Add(new Students(6, "Alo 6", 19, "fr_net"));
            listStudent.Add(new Students(5, "Hihi 5", 17, "fr_test"));
            listStudent.Add(new Students(3, "Khanh 3", 20, "fr_net"));
            listStudent.Add(new Students(7, "AAAA 7", 20, "fr_test"));
            return listStudent;
        }
        // Hàm kiểm tra sự tồn tại của student-- dựa vào ID của student
        public static void IsExist(List<Students> list, Students student)
        {
            var result = list.Where(x => x.StudentId == student.StudentId).Any();
            if (result == true)
            {
                System.Console.WriteLine("Co ton tai !");
            }
            else
            {
                System.Console.WriteLine("Khong ton tai !");
            }
        }
        // Hàm sắp xếp: Có thể sắp xếp theo các thuộc tính của student truyền vào
        public static void Sort(List<Students> list, string prop)
        {
            if (list.Count() >= 2)
            {
                var comparer = new StudentCompare(prop);
                list.Sort(comparer);
            }
            else
            {
                System.Console.WriteLine("Khong du dieu kien de sap xep!");
            }
        }
        //Hàm xóa phần tử dựa vào ID của student
        public static void Delete(List<Students> list, Students student)
        {
            try
            {
                list.Remove(list.Where(x => x.StudentId == student.StudentId).First());
                System.Console.WriteLine("Xoa thanh cong!");
            }
            catch
            {
                System.Console.WriteLine("Xoa that bai!");
            }
        }

        //Đếm lại số lượng
        public static int Count(List<Students> list)
        {
            return list.Count();
        }
    }
}
