namespace Baitap
{
    public class Student
    {
        public string Name{get;set;}
        public int  Age{get;set;}
        public string IDClass{get;set;}
        public Student(string name, int age, string idclass)
        {
            Name=name;
            Age= age;
            IDClass= idclass;
        }
    }
}