namespace Baitap
{
    public class Class
    {
        public string IDClass{get;set;}
        public string Name{get;set;}
        public Class(string idclass, string name)
        {
            IDClass=idclass;
            Name=name;
        }
    }
}