﻿using System.Diagnostics.SymbolStore;
using System.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Baitap
{
    class Program
    {
        static void Main(string[] args)
        {
            var lstStudent = CreateStudent();
            var lstClass = CreateClass();

            //Show Classes and Students.
            System.Console.WriteLine("In ra danh sach hoc sinh theo lop");
            var group = lstClass.GroupJoin(
                lstStudent,
                cls => cls.IDClass,
                stu => stu.IDClass,
                (cls, stu) => new { cls, stu });
            foreach (var g in group)
            {
                System.Console.WriteLine($"Class: {g.cls.Name}");
                foreach (var i in g.stu)
                {
                    System.Console.WriteLine($"\tName: {i.Name},  Age: {i.Age}.");
                }
            }
            // Combine List of Student
            System.Console.WriteLine("\nTao chuoi danh sach hoc sinh theo lop");
            foreach (var g in group)
            {
                var tmp="";
                 tmp =  g.stu.Select(x=>x.Name).Aggregate<string>((x,y)=>x+ ",   "+y);
                 System.Console.WriteLine($"Class {g.cls.Name}: {tmp}");
            }

            //Count Students
            System.Console.WriteLine("\nDem so luong hoc sinh theo lop");
            var count = lstClass.GroupJoin(
                lstStudent,
                cls => cls.IDClass,
                stu => stu.IDClass,
                (cls, stu) => new { Name = cls.Name, Number = stu.Count() });
            foreach (var i in count)
            {
                System.Console.WriteLine($"Name: {i.Name}: {i.Number}.");
            }
        }

        public static List<Student> CreateStudent()
        {
            var listStudent = new List<Student>();
            listStudent.Add(new Student("Khanh 1", 18, "fr_net"));
            listStudent.Add(new Student("Khanh 2", 19, "fr_test"));
            listStudent.Add(new Student("Khanh 3", 20, "fr_net"));
            listStudent.Add(new Student("Khanh 4", 21, "fr_net"));
            listStudent.Add(new Student("Khanh 5", 17, "fr_test"));
            listStudent.Add(new Student("Khanh 6", 19, "fr_net"));
            listStudent.Add(new Student("Khanh 7", 20, "fr_test"));
            listStudent.Add(new Student("Khanh 8", 23, "fr_test"));
            return listStudent;
        }
        public static List<Class> CreateClass()
        {
            var listClass = new List<Class>();
            listClass.Add(new Class("fr_net", "Fresher DotNet"));
            listClass.Add(new Class("fr_test", "Fresher AutoTest"));
            return listClass;
        }
    }

}
