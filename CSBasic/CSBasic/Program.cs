﻿using System;
using System.Linq;

namespace CSBasic
{
    public class Program
    {
        static void Main(string[] args)
        {
            var inputArray = new[] { 1, 1, 1, 1, 1 };
            var s = CheckArray(inputArray);
            Console.WriteLine(s);
        }
        public static int CountString(string inputString, string subString)
        {
            var split = inputString.Split(subString);
            var result = split.Length;
            return result - 1;
        }
        public static string ReplaceString(ref string inputString, params char[] replaceChar)
        {
            for (int i = 0; i < replaceChar.Length; i++)
            {
                inputString = inputString.Replace(replaceChar[i].ToString(), "NET05");
            }
            return inputString;
        }
        public static string CompareArrayWithCurrentYear(int[] inputArray)
        {
            var sum = inputArray.Sum();
            if (sum > DateTime.Now.Year)
            {
                return "Higher";
            }
            return "Lower";
        }
        public static string CheckArray(int[] inputArray)
        {
            if (inputArray.Contains(0))
            {
                return "Array must not containts Zero";
            }
            var evenArray = inputArray.Where(x => x % 2 == 0);
            var oddArray = inputArray.Except(evenArray);

            var sumEven = evenArray.Sum();
            var sumOdd = oddArray.Sum();

            var countEven = evenArray.Count();
            var countOdd = oddArray.Count();
            if (sumEven >= sumOdd)
            {
                return countEven > countOdd ? "Even-Even" : "Even-Odd";
            }
            else
            {
                return countOdd > countEven ? "Odd-Odd" : "Odd-Even";
            }
        }
    }
}
