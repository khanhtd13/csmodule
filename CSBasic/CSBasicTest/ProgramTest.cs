using System;
using Xunit;
using CSBasic;

namespace CSBasicTest
{
    public static class ProgramTest
    {
        [Theory]
        [InlineData("FPT Software Company HoChiMinh","iiii",0)]
        [InlineData("FPT Software Company HoChiMinh","", 0)]
        [InlineData("FPT Software Company HoChiMinh","FPT",1)]
        [InlineData("","",0)]
        [InlineData("","Something",0)]
        public static void CountStringTest(string inputString,string subString,int expect)
        {
            var result = Program.CountString(inputString,subString);
            Assert.Equal(expect,result);
        }

        [Theory]
        [InlineData("Day la chuoi ban dau nhe", "DNET05y lNET05 chuoi bNET05n dNET05u nhe", 'a')]
        [InlineData("Day la chuoi ban dau nhe", "Day NET05a chNET05oi ban daNET05 nhe", 'l','u')]
        [InlineData("FPT Software Company HoChiMinh", "FPTNET05SoftwareNET05CompanyNET05HoChiMinh", ' ' )]
        [InlineData("","",' ' )]
        public static void ReplaceString(ref string inputString,string expect,params char[] replaceChar)
        {
            var result = Program.ReplaceString(ref inputString, replaceChar);
            Assert.Equal(expect, result);
        }
        [Theory]
        [InlineData(new[] {1,2,3,4,5 }, "Lower")]
        [InlineData(new[] {10000,2,3,4,5 }, "Higher")]
        [InlineData(new[] {-10000,2,3,4,5 }, "Lower")]
        public static void CompareArrayWithCurrentYear(int[] inputArray,string expect)
        {
            var result = Program.CompareArrayWithCurrentYear(inputArray);
            Assert.Equal(expect, result);
        }

        [Theory]
        [InlineData(new[] { 0, 2, 3, 4, 5 }, "Array must not containts Zero")]
        [InlineData(new[] { 12, 2, 3, 10, -5 }, "Even-Even")]
        [InlineData(new[] { 12, 1, 3, 10, 5 }, "Even-Odd")]
        [InlineData(new[] { 2, 2, 13, 4, 5 }, "Odd-Even")]
        [InlineData(new[] { 2, 1, 13, 4, 5 }, "Odd-Odd")]
        [InlineData(new[] { -12, 1, 3, 4, 5 }, "Odd-Odd")]
        public static void CheckArray(int[] inputArray, string expect)
        {
            var result = Program.CheckArray(inputArray);
            Assert.Equal(expect, result);
        }

    }
}
